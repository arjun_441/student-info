package com.sort;

import java.util.Arrays;

/**
 * @Author arjun
 * @Date 6/21/18
 **/
public class Student {
    public String[] getStudent() {
        return student;
    }

    public void setStudent(String[] student) {
        this.student = student;
    }

    private String student[];

    public Student() {
    }


    public void display() {
        Arrays.sort(student);
        Arrays.stream(student).forEach(System.out::println);

//        System.out.println();    }

    }
}
