package com.sort;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Arrays;
import java.util.Scanner;

/**
 * @Author arjun
 * @Date 6/21/18
 **/
public class StudentInfo {


    public static void main(String[] args) {
        System.out.println();
        Scanner sc = new Scanner(System.in);
        System.out.println("Please enter the number of Students you wanna sort");
        int n = sc.nextInt();
//        System.out.println(n);


        String a1[] = new String[n];
        System.out.println("Enter" + " "+n +" "+ "student names please?");

        for (int i = 0; i < a1.length; i++) {
             a1[i] = sc.next();
        }

//        Arrays.sort(a1);

        ApplicationContext applicationContext =
                new ClassPathXmlApplicationContext("ApplicationContext.xml");
        Student student = (Student) applicationContext.getBean("a3");
        student.setStudent(a1);
        student.display();



//        System.out.println(a1);
    }
}